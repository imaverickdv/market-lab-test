FROM node:20-alpine

ENV NODE_ENV=dev

WORKDIR /market-lab-test

COPY ["package.json", "package-lock.json", "/market-lab-test/"]

RUN npm install

COPY . .

CMD ["npm", "run", "start:dev"]