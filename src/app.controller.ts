import { Body, Controller, Get, Param, Post } from '@nestjs/common';
import { AbstractController } from '@app/common';
import { AppService } from "@app/app.service";
import { LinkPostDto } from "@app/dto/link-post.dto";

@Controller()
export class AppController extends AbstractController {
    constructor(
        private appService: AppService,
    ) {
        super()
    }

    /*
     1. Создать одноразовую ссылку
        принимает строку, запоминает её и возвращает ссылку,
        по которой можно получить строку, одноразовая ссылка должна быть уникальна,
        т.е. в один момент времени, в сервисе не может быть 2х одинаковых активных одноразовых ссылок.
     */
    @Post('/link')
    public async postLink(
        @Body() payload: LinkPostDto,
    ) {
        try {
            return this.sendResponse(this.appService.createLink(payload));
        } catch (error: any) {
            return this.sendResponse(null, error.message)
        }
    }

    /*
    2. Получение значения по одноразовой ссылке, сгенерированной в 1-м эндпойнте.
       При получении значения по одноразовой ссылке необходимо проверять, активна ли она.
       Если ссылка уже использована, то следует вернуть сообщение об ошибке.
     */
    @Get('/link/:code')
    public async getLink(
        @Param('code') code: string,
    ) {
        try {
            return this.sendResponse(this.appService.getLink(code));
        } catch (error: any) {
            return this.sendResponse(null, error.message)
        }
    }
}
