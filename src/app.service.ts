import { Injectable } from '@nestjs/common';
import { LinkPostDto } from "@app/dto/link-post.dto";
import { v4 as uuidv4 } from 'uuid';
import { ConfigService } from "@nestjs/config";

@Injectable()
export class AppService {
    private storage: Map<string, string>;

    constructor(
        private configService: ConfigService,
    ) {
        this.storage = new Map();
    }

    public createLink(payload: LinkPostDto): string {
        if (!payload.q) {
            throw new Error('Empty string');
        }

        let uniqueId = uuidv4();

        while (this.storage.has(uniqueId)) {
            uniqueId = uuidv4();
        }

        this.storage.set(uniqueId, payload.q);

        return `127.0.0.1:${this.configService.get<string>('SERVER_PORT')}/link/${uniqueId}`;
    }

    public getLink(uid: string): string {
        if (!this.storage.has(uid)) {
            throw new Error('Not found');
        }

        return this.storage.get(uid)!;
    }
}
