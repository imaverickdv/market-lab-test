import { Request } from 'express';
import crypto from 'crypto';
import { ApiResponse } from "@common/api-response";

export class AbstractController {
    protected sendResponse(data?: any, errorMessage?: string) {
        const response = new ApiResponse();
        return errorMessage ? response.error(errorMessage, data) : response.ok(data);
    }
}