export class ApiResponse<T = any> {
    success: boolean;
    data?: T;
    message?: string;

    error(errorMessage: string, data?: T) {
        if (data) {
            this.data = data;
        }

        this.message = errorMessage;
        this.success = false;
        return this;
    }

    ok(data: T) {
        this.data = data;
        this.success = true;
        return this;
    }
}