import 'module-alias/register';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { useContainer } from 'class-validator';
import { NestExpressApplication } from '@nestjs/platform-express';

async function bootstrap() {
    const app = await NestFactory.create<NestExpressApplication>(AppModule);

    app.enableCors({
        origin: '*',
        methods: 'GET,HEAD,PUT,PATCH,POST,DELETE',
        credentials: true,
    });

    useContainer(app.select(AppModule), { fallbackOnErrors: true });

    await app.listen(process.env.SERVER_PORT as string, () => {
        console.log(`listening on: ${process.env.SERVER_PORT}`);
    });
}

bootstrap();
